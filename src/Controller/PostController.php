<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\Thread;
use App\Form\EditCommentType;
use App\Form\EditPostType;
use App\Repository\PostRepository;
use App\Repository\ThreadRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


class PostController extends AbstractController
{
    /**
     * @Route("/p={id}/edit/", name="edit_post", methods={"GET", "POST"})
     */
    public function editPost(Post $post, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $form = $this->createForm(EditCommentType::class, $post);

        if ($user !== null) {
            if ($post !== null) {
                if ($user == $post->getUser()) {
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $post->setContent($form['content']->getData());
                        $entityManager->persist($post);
                        $entityManager->flush();

                        return $this->redirectToRoute('thread', ['id' => $post->getThread()->getId()]);
                    }

                    return $this->render('post/edit.html.twig', [
                        'post' => $post,
                        'form' => $form->createView()
                    ]);
                }
                return $this->redirectToRoute('404');
            }
            return $this->redirectToRoute('404');
        }
        return $this->redirectToRoute('default');
    }

    /**
     * @Route("/p/create/comment/{id}/", name="create_post")
     */
    public function createPost(Thread $thread, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $form = $this->createForm(EditPostType::class);


//        if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
//            if ($user->getApproved() == false) {
//                return $this->redirectToRoute('perms', []);
//            }
//            return $this->redirectToRoute('404');
//        }

        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $post = new Post();
                $post->setUser($user);
                $post->setContent($form['content']->getData());
                $post->setThread($thread);
                $entityManager->persist($post);
                $entityManager->flush();

                return $this->redirectToRoute('thread', [
                    'id' => $thread->getId()
                ]);
            }
        }
        return $this->render('post/create.html.twig', [
            'form' => $form->createView(),
            'thread' => $thread
        ]);

    }
}
