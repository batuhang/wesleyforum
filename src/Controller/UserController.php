<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\Thread;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboard(User $user){

        return $this->render('user/dashboard.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/profile/myposts/", name="user_profile_show", methods={"GET"})
     */
    public function profile(){
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if($user !== null){
            if ($user->getApproved() == false){
                return $this->redirectToRoute('perms', []);
            }
        }

        $post = $this->getDoctrine()
            ->getRepository(Post::class)
            ->findBy(['user' => $user], ['id' => 'DESC']);

        $thread = $this->getDoctrine()
            ->getRepository(Thread::class)
            ->findBy(['user' => $user], ['id' => 'DESC']);

        return $this->render('user/profile.html.twig', [
            'user' => $user,
            'posts' => $post,
            'threads' => $thread
        ]);
    }
}
