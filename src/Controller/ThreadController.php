<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Post;
use App\Entity\Thread;
use App\Form\EditPostType;
use App\Repository\PostRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

use Knp\Component\Pager\PaginatorInterface;

class ThreadController extends AbstractController
{
    /**
     * @Route("/c={id}/", name="thread_category", methods={"GET"})
     */
    public function categories(Category $category)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            if ($user->getApproved() == false) {
                return $this->redirectToRoute('perms', []);
            }
        }

        $threads = $this->getDoctrine()
            ->getRepository(Thread::class)
            ->findBy(['category' => $category]);

        return $this->render('thread/categories.html.twig', [
            'threads' => $threads
        ]);
    }

    /**
     * @Route("t={id}", name="thread", methods={"GET", "POST"})
     */
    public function show($id, Request $request, PaginatorInterface $paginator, PostRepository $postRepository): Response
    {
        $securityContext = $this->container->get('security.authorization_checker');
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $entityManager = $this->getDoctrine()->getManager();

        $thread = $this->getDoctrine()
            ->getRepository(Thread::class)
            ->findOneBy(['id' => $id]);

        $form = $this->createForm(EditPostType::class);

        $posts = $postRepository->getByThread($thread);

        $pagination = $paginator->paginate(
            $posts,
            $request->query->getInt('page', 1), /*page number*/
            5 /*limit per page*/
        );

        if (empty($thread)) {
            return $this->redirectToRoute('404');
        }

        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $post = new Post();
                $post->setUser($user);
                $post->setContent($form['content']->getData());
                $post->setThread($thread);
                $entityManager->persist($post);
                $entityManager->flush();

                return $this->redirectToRoute('thread', [
                    'id' => $thread->getId()
                ]);
            }
        }

        return $this->render('thread/index.html.twig', [
            'thread' => $thread,
            'form' => $form->createView(),
            'pagination' => $pagination
        ]);
    }
}
