<?php

namespace App\Controller;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index()
    {
        $securityContext = $this->container->get('security.authorization_checker');
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            if ($user->getApproved() == false) {
                return $this->redirectToRoute('perms', []);
            }
        }
        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        return $this->render('default/index.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/notfound", name="404")
     */
    public function notFound()
    {
        return $this->render('error/404.html.twig', [

        ]);
    }

    /**
     * @Route("/noperms", name="perms")
     */
    public function noPerms()
    {
        return $this->render('error/permission.html.twig', [

        ]);
    }
}
